#include <Wire.h>
#include "DFRobot_INA219.h"


// // PINS ======================================================================

// Moteur
#define dirPin 4
#define stepPin 5

// Pins d'interruption
const byte interruptPinHaut = 2; // Plateforme tt en haut
const byte interruptPinBas = 3; // Plateforme au niveau de l'échantillon
const byte sleepModePin = 6;

// // VARIABLES GLOBALES ========================================================
// Type de mesure
boolean van_der_pauw = true;

// Moteur
#define stepsPerRevolution 200 // nombre de steps / révolution

// Variables pour les routiles d'interruption
volatile byte consigne_haut = 1;
volatile byte consigne_bas = 1;

// Grandeurs pour le calcul de la résistivité
double courant_mA = 0 ;
double epaisseur_mm = 0 ;
double courant_A = 0 ;
double epaisseur_m = 0 ;

// // WATTMETRES ================================================================

// Wattmètres
DFRobot_INA219_IIC     ina219_tension(&Wire, INA219_I2C_ADDRESS2);
DFRobot_INA219_IIC     ina219_courant(&Wire, INA219_I2C_ADDRESS4);

// Calibration avec une résistance de 10 ohms du wattmètre courant
float ina219Reading_mA = 103;
float extMeterReading_mA = 106;

// // FUNCTIONS =================================================================

// Affichage / demande de valeurs  ............................................

double demande_valeur(String grandeur, String unite){
  // Demande une valeur à l'utilisateur via le Serial.
  Serial.print("Saisir ");
  Serial.print(grandeur);
  Serial.print(" (");
  Serial.print(unite);
  Serial.println(" ) :");
  while (Serial.available()<=0); // on attend une valeur
  char Buffer[8];
  int x=0;
  
  while (Serial.available() > 0) { // lecture
    Buffer[x]=Serial.read();
    delay(10);
    x++;
  }
  double valeur = atof(Buffer);
  Serial.print(valeur);
  Serial.print(" ");
  Serial.println(unite);

  return valeur;
}

void lire_commande(int* etat, double* epaisseur_mm) {
  delay(100);
  char commande = Serial.read();
  
  if (commande=='C') {
    // Serial.println("Calcul");
    *etat = 2;
    Serial.read() ; // caractère ':'
    char Buffer[8];
    int x=0;
    while (Serial.available() > 0) { // lecture de l'épaisseur
      Buffer[x]=Serial.read();
      delay(10);
      x++;
    }
    *epaisseur_mm = atof(Buffer)  ;
    Serial.print("Epaisseur : ");
    Serial.println(*epaisseur_mm);
  } else if (commande=='S') {
    *etat = -1;
    Serial.println("demarrage stop");
  } else if (commande=='M') {
    *etat = 0;
    Serial.println("demarrage montee");
    
  } else if (commande=='D') {
    *etat = 1;
    Serial.println("demarrage descente");
  }
  // Le reste est jeté
  while (Serial.available() > 0) {
    Serial.read();
    delay(10);
  }
  
  
}


void print_quantite(String nom, float number, String unite, float error=-1.)
{
  // Fonction pour afficher le résultat d'une grandeur physique, avec erreur
  
  Serial.print(nom);
  Serial.print(": ");
  Serial.print(number, 5);

  if (error!=-1.) {
    Serial.print(" +/- ");
    Serial.print(error, 5);
  } 

  Serial.print(" ");
  Serial.println(unite);
}

void print_quantite_python(String nom, float number)
{
  // Fonction pour afficher le résultat d'une grandeur physique, pour python
  
  Serial.print(nom);
  Serial.print(":");
  Serial.println(number,5); 
}

// Calcul de la résistivité ..................................................

//double mesure_resistivite(double tension_V, double courant_A, double epaisseur_m) {
//  // Calcul la résistivité pour la mesure 4 pointes alignées
//  double C = 4.532360;
//
//  double rho = C * epaisseur_m * ( tension_V / courant_A );
//  return rho;
//}
//
//
//double mesure_resistivite_van_der_pauw(double tension_V, double courant_A, double epaisseur_m) {
//  // Hypothèse: R_AB,CD = R_AC,BD
//  double C = 4.53236; // pi / ln(2)
//  return C * epaisseur_m * (tension_V / courant_A);
//}

// Initialisation wattmètres ..................................................

void initiliate_ina219(DFRobot_INA219_IIC ina219, float ina219_reading, float extMeterReading)
{
  // Initialise le wattmètre ina219
  while(!Serial);
  
  Serial.println();
  while(ina219.begin() != true) {
      Serial.println("INA219 begin failed");
      delay(2000);
  }
  ina219.linearCalibrate(ina219_reading, extMeterReading);
  Serial.println();
}

// Routines d'interruption ...................................................
void detection_haut() {
    consigne_haut = 0;
    consigne_bas = 1;
}

void detection_bas() {
    consigne_bas = 0;
}

// // MAIN =================================================================

int etat = -1;
/*
 * etat 0: Montée du moteur
 * etat 1: Descente du moteur
 * etat 2: Mesure de résistivité
 * etat 3: Montée du moteur et arrêt
 */

void setup(void) 
{

  // Pins ----------------------------------------------------------------
  // Controle moteur  
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);

  // Interruptions
  pinMode(interruptPinHaut, INPUT);
  pinMode(interruptPinBas, INPUT);

  pinMode(sleepModePin, OUTPUT);
  digitalWrite(sleepModePin, LOW);
  // Initilisation du SERIAL et wattmètres ------------------------------
  Serial.begin(115200);
  initiliate_ina219(ina219_courant, ina219Reading_mA, extMeterReading_mA);
  initiliate_ina219(ina219_tension, ina219Reading_mA, extMeterReading_mA);


  // Demande de valeurs -------------------------------------------------
  // On demande l'épaisseur à l'utilisateur, car cette dernière n'est pas
  // mesurée.
  
  // courant_mA = demande_valeur("courant", "mA");
  // epaisseur_mm = demande_valeur("épaisseur", "mm");
  
  // courant_A = courant_mA / 1000. ;
  // epaisseur_m = epaisseur_mm / 1000. ;
}


void loop(void)
{

  
  if (Serial.available()>0) {
    lire_commande(&etat, &epaisseur_mm);
    epaisseur_m = epaisseur_mm / 1000. ; 
  }

  if (etat==0) {
    // Montée plateau ----------------------------------------------------------------
    //consigne_haut = !digitalRead(interruptPinHaut);
    //consigne_bas = !digitalRead(interruptPinBas);
    //Serial.print("Consigne haut ");
    //Serial.println(consigne_haut);
    
    digitalWrite(sleepModePin, HIGH);
    consigne_haut = 1;
    while (consigne_haut == 1) { // tant qu'on a pas atteint une butée
      // Set the spinning direction clockwise (go up)
      digitalWrite(dirPin, LOW);
      
      // Move the system upwards
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(1100);
      digitalWrite(stepPin, LOW); 
      delayMicroseconds(1100);
  
      //attachInterrupt(digitalPinToInterrupt(interruptPinHaut), detection_haut, CHANGE);
      
      if (Serial.available()) {
        char Buffer[8];
        int x=0;
        while (Serial.available() > 0) { // lecture de l'épaisseur
          Buffer[x]=Serial.read();
          delay(10);
          x++;
        }
        if (strcmp(Buffer[0],"S")){
          consigne_haut = 0;
        }
        
          
      }
    }
    Serial.println("S");
    etat = -1;
    digitalWrite(sleepModePin, LOW);

    // Arrivée tt en haut
//    if (etat==3) {
//      while(1);
//    } else if (etat==0) {
//      etat = 1; // on passe à la descente
//    }
    
  } else if (etat==1) {
    //consigne_haut = !digitalRead(interruptPinHaut);
    //consigne_bas = !digitalRead(interruptPinBas);
    consigne_bas = 1;
    // Descente plateau ----------------------------------------------------------------
    digitalWrite(sleepModePin, HIGH);
    while (consigne_bas == 1) { // tant qu'on a pas atteint une butée
      // Set the spinning direction counterclockwise (go down)
      digitalWrite(dirPin, HIGH);
  
      // Move the system downwards
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(1100);
      digitalWrite(stepPin, LOW); 
      delayMicroseconds(1100);
  
      //attachInterrupt(digitalPinToInterrupt(interruptPinBas), detection_bas, CHANGE);

      if (Serial.available()) {
        char Buffer[8];
        int x=0;
        while (Serial.available() > 0) { // lecture de l'épaisseur
          Buffer[x]=Serial.read();
          delay(10);
          x++;
        }
        if (strcmp(Buffer[0],"S")){
          consigne_bas = 0;
        }
          
      }
    }
    digitalWrite(sleepModePin, LOW);
    Serial.println("S");
    etat = -1;
  } else if (etat==2) {
    // Mesure de résistivité ----------------------------------------------------------

    // Mesure de la tension ..................................
    double tension_erreur_V = 4 / 1000 ; // 4 mV
    double tension_V = ina219_tension.getBusVoltage_V();
  
    //print_quantite("tension", tension_V, "V", tension_erreur_V);
    print_quantite_python("V", tension_V);
    // Mesure du courant .....................................
    double courant_erreur_mA = 1 ; // 1 mA
    double courant_erreur_A = courant_erreur_mA / 1000 ; // 1 mA
    courant_mA = ina219_courant.getCurrent_mA();
    courant_A = courant_mA / 1000.;
    //print_quantite("courant", courant_mA, "mA", courant_erreur_mA);
    print_quantite_python("I", courant_mA);
    
    

    // Calcul de la résistivité ...............................
  
//    double resistivite_ohmm;
//    if (van_der_pauw) {
//      resistivite_ohmm = mesure_resistivite_van_der_pauw(tension_V, courant_mA, epaisseur_mm);
//      
//    } else {
//      resistivite_ohmm = mesure_resistivite(tension_V, courant_mA, epaisseur_mm);
//    }
//  
//    Serial.print("==> ");
//    print_quantite("resistivité", resistivite_ohmm, "ohm.m");

    Serial.println("");

    delay(1000);
 }

}
