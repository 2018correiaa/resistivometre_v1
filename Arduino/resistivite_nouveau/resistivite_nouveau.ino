/* Mesure de la résistivité d'un échantillon à l'aide d'une méthode 4 pointes.
Ce Programme est destiné à fonctionné avec le programme python associé.

Il s'agit d'une machine d'état (état donné par `etat` (int)).

Il a 4 états possibles:
 * etat -1: rien faire
 * etat 0: Montée du moteur
 * etat 1: Descente du moteur
 * etat 2: Mesure de résistivité --> renvoie les valeurs de tension et intensité
      toutes les secondes.

La mesure de la résistivité est réalisée par le programme python.
*/

#include <Wire.h>
#include "DFRobot_INA219.h"


// // PINS ======================================================================

// Moteur ---------------------------------------
// Contrôle
#define dirPin 4
#define stepPin 5

// Pin à mettre à 0 si le moteur est inactif, pour le mettre en mode 'sleep'
#define activeModePin 6

// // VARIABLES GLOBALES ========================================================
// Moteur ----------------------------------------
#define stepsPerRevolution 200 // nombre de steps / révolution

// Grandeurs pour le calcul de la résistivité ----
double courant_mA = 0 ;
double epaisseur_mm = 0 ;
double courant_A = 0 ;
double epaisseur_m = 0 ;

// // WATTMETRES ================================================================

// Wattmètres
DFRobot_INA219_IIC     ina219_tension(&Wire, INA219_I2C_ADDRESS2);
DFRobot_INA219_IIC     ina219_courant(&Wire, INA219_I2C_ADDRESS4);

// Calibration avec une résistance de 10 ohms du wattmètre courant `ina219_courant`
float ina219Reading_mA = 103;
float extMeterReading_mA = 106;

// // FUNCTIONS =================================================================

// Affichage / demande de valeurs  ............................................

double demande_valeur(String grandeur, String unite){
  /* Demande une valeur à l'utilisateur via le Serial.
   * Non utilisé dans le `main()` car j'utilise le programme python
     pour récupérer la valeur de l'épaisseur.
    
    Affiche "Saisir {grandeur} ({unit}):" et renvoie
    le résultat (limité à 9 caractères).
    
     Args:
      grandeur: nom de la grandeur qu'on veut récupérer
      unite   : unité de la grandeur qu'on veut récupérer

    Returns:
      valeur: valeur saisie par l'utilisateur.
    
  */ 
  Serial.print("Saisir ");
  Serial.print(grandeur);
  Serial.print(" (");
  Serial.print(unite);
  Serial.println(" ) :");
  while (Serial.available()<=0); // on attend une valeur
  char Buffer[8];
  int x=0;
  
  while (Serial.available() > 0) { // lecture
    Buffer[x]=Serial.read();
    delay(10);
    x++;
  }
  double valeur = atof(Buffer);
  Serial.print(valeur);
  Serial.print(" ");
  Serial.println(unite);

  return valeur;
}

void dump_serial() {
  /* Ignore ce qui a été écrit dans le Serial. */
  while (Serial.available() > 0) {
    delay(10);
  }
}

void lire_commande(int* etat) {
  /*  Lit la commande envoyée via la Serial (typiquement
  par la progamme python) et change l'état actuel de
  la machine d'état.

  Args:
    etat (pointeur): état actuel de la machine d'état 

  Les commandes possibles sont:
  - `C`: Mesure de la résistivité, passage à l'état 2
  - `S`: Stop: passage à l'état -1  
  - 'M': Montée: passage à l'état 0
  - 'D': Descente: passage à l'état 1
  */

  // delay pour que la commande complète est le temps d'arriver
  delay(100); 
  
  
  char commande = Serial.read(); // Lecture de la commande
  
  if (commande=='C') {
    // Serial.println("Calcul");
    *etat = 2;
    
    
  } else if (commande=='S') {
    *etat = -1;
    // Serial.println("demarrage stop");
  } else if (commande=='M') {
    *etat = 0;
    Serial.println("demarrage montee");
    
  } else if (commande=='D') {
    *etat = 1;
    // Serial.println("demarrage descente");
  }
  
  // Le reste est jeté  
  dump_serial();
}

bool is_commande_stop() {
  /*
  Lire le Serial, et renvoie `true`
  si le premier caractère envoyé est `S`,
  qui signifierait ``STOP`.

  returns:
    is_commande_stop (bool): `true` si la commande
      stop a été envoyée.    
   */
  if (Serial.available()) {
    // Lire la commande
    char commande = Serial.read(); // lire le premier caractère
    dump_serial();
        
    if (strcmp(commande,"S")){
      return true;
    }
  } 
  
  return false;
}

void print_quantite(String nom, float number, String unite, float error=-1.)
{
  /*
  Fonction pour afficher le résultat d'une grandeur physique, avec erreur
  Elle n'est pas utilisée ici mais elle peut être pratique donc je la laisse.
  Elle affiche: "{nom} : {number} +/- {error} {unite}"
  ou bien "{nom} : {number} {unite}" si l'erreur n'est pas donnée.

  Args:
    nom: nom de la quantité
    number: valeur de la quantite
    unite: unité de la quantité
    error: si spéicifiée, valeur de l'erreur sur la quantité  
  */
  
  Serial.print(nom);
  Serial.print(": ");
  Serial.print(number, 5);

  if (error!=-1.) {
    Serial.print(" +/- ");
    Serial.print(error, 5);
  } 

  Serial.print(" ");
  Serial.println(unite);
}

void print_quantite_python(String nom, float number)
{
  /* 
  Fonction pour afficher le résultat d'une grandeur physique, dans un format
  compressé. Je l'utilise pour envoyer les valeurs de courants et tensions
  via le Serial (pour le programme python).
  Il envoie "{nom}:{numbeer}".

  Args:
    nom: nom de la quantité
    number: valeur de la quantite
  */
  
  Serial.print(nom);
  Serial.print(":");
  Serial.println(number,5); 
}

// Calcul de la résistivité ..................................................

double mesure_resistivite(double tension_V, double courant_A, double epaisseur_m) {
  /* Calcul la résistivité pour la mesure 4 pointes alignées
  ou dans la mesure de Van Der Pauw (pour une seule mesure).

  Cette fonction n'est pas du tout utilisée dans `main ()` car c'est le programme
  python qui calcule la résistivité. Je la laisse ici pour tests.

  Args:
    tension_V: tension en volt
    courant_A: intensité en ampère
    epaisseur_m : épaisseur en mètre

  Note:
    on peut aussi fournir le courant en mA et l'épaisseur en mm.
  */
  double C = 4.532360;

  double rho = C * epaisseur_m * ( tension_V / courant_A );
  return rho;
}




// Initialisation wattmètres ..................................................

void initiliate_ina219(DFRobot_INA219_IIC ina219, float ina219_reading, float extMeterReading)
{
  /* 
  Initialise le wattmètre ina219

  Args:
    ina219: l'objet wattmètre
    ina219_reading : valeur mesurée par le wattmètre lors de la calibration
    extMeterReading: valeur mesurée par un ampèremètre extérieur lors de la calibration  
   */
  while(!Serial);
  
  Serial.println();
  while(ina219.begin() != true) {
      Serial.println("INA219 begin failed");
      delay(2000);
  }
  ina219.linearCalibrate(ina219_reading, extMeterReading);
  Serial.println();
}

// // MAIN =================================================================

// Etat de la machine d'état (initialement, ne fait rien)
int etat = -1;

void setup(void) 
{

  // Pins ----------------------------------------------------------------
  // Controle moteur  
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(activeModePin, OUTPUT);
  digitalWrite(activeModePin, LOW); // initialement, moteur en mode "sleep"
  
  // Initilisation du SERIAL et wattmètres ------------------------------
  Serial.begin(115200); // NB: taux de communication à 115200 bauds!
  initiliate_ina219(ina219_courant, ina219Reading_mA, extMeterReading_mA);
  initiliate_ina219(ina219_tension, ina219Reading_mA, extMeterReading_mA);

  // Demande de valeurs -------------------------------------------------
  // On peut demande l'épaisseur à l'utilisateur
  // Ici on utilise le programme python
  // epaisseur_mm = demande_valeur("épaisseur", "mm");
  // epaisseur_m = epaisseur_mm / 1000. ;
}


void loop(void)
{

  // On lit la commande envoyée via le Serial (par le programme python par exemple
  // Il peut éventuellement nous demander de changer d'état.
  if (Serial.available()>0) {
    lire_commande(&etat);
    // epaisseur_m = epaisseur_mm / 1000. ; 
  }

  if (etat==0 or etat==1) {
    // Montée/descente plateau ----------------------------------------------------------------
    
    digitalWrite(activeModePin, HIGH); // moteur en mode actif

    
    bool pas_stop = true;
    
    if (etat==0) {
      // Set the spinning direction clockwise (go up)
      digitalWrite(dirPin, LOW);
    } else if (etat==1) {
      // Set the spinning direction counterclockwise (go down)
      digitalWrite(dirPin, HIGH);
    }

    // tant qu'on nous demande pas d'arrêter
    while (pas_stop) {
      // Move the system upwards or downwards
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(1100);
      digitalWrite(stepPin, LOW); 
      delayMicroseconds(1100);

      pas_stop = !is_commande_stop();
    }

    // Si on s'arrête, en va à l'état -1 et envoie `S`
    // au programme python pour qu'il sache que le moteur s'est arrêté.
    Serial.println("S");
    etat = -1;

    // Le moteur revient en mode sleep
    digitalWrite(activeModePin, LOW);
    
  } else if (etat==2) {
    // Mesure de résistivité ----------------------------------------------------------

    // Mesure de la tension ..................................
    // double tension_erreur_V = 4 / 1000 ; // 4 mV
    double tension_V = ina219_tension.getBusVoltage_V();
  
    //print_quantite("tension", tension_V, "V", tension_erreur_V);
    // on communique la tension au programme python
    print_quantite_python("V", tension_V);
    
    // Mesure du courant .....................................
    //double courant_erreur_mA = 1 ; // 1 mA
    //double courant_erreur_A = courant_erreur_mA / 1000 ; // 1 mA
    courant_mA = ina219_courant.getCurrent_mA();
    //courant_A = courant_mA / 1000.;
    //print_quantite("courant", courant_mA, "mA", courant_erreur_mA);
    // on communique le courant en mA au programme Arduino
    print_quantite_python("I", courant_mA);
    
    
    // Calcul de la résistivité ...............................
  
//    double resistivite_ohmm;
//    if (van_der_pauw) {
//      resistivite_ohmm = mesure_resistivite_van_der_pauw(tension_V, courant_mA, epaisseur_mm);
//      
//    } else {
//      resistivite_ohmm = mesure_resistivite(tension_V, courant_mA, epaisseur_mm);
//    }
//  
//    Serial.print("==> ");
//    print_quantite("resistivité", resistivite_ohmm, "ohm.m");

    delay(1000); // la mesure est actualisée toutes les secondes
 }

}
