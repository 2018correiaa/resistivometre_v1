# Résistivomètre (v1)

Ce git héberge le programme python et arduino du résistivomètre, utilisée la mesurée 4 pointes (alignées ou en mode Van der Pauw) pour la mesure de la résistivité.
Ce projet a été réalisé dans le cadre de l'activité "Prototypage en Fablab" de la mentions QTE.

## Machine d'état

L'Arduino se comporte comme une machine d'état, donnée par l'état `etat` qui vaut:
- `-1`: à l'état inactif
- `0` : lorsque le moteur fait monter la plateforme
- `1` : lorsque le moteur fait descendre la plateforme
- `2` : lorsque l'Arduino mesure la valeur de l'intensité et de la tension à l'aide des 2 wattmètres. Elle renvoie ensuite la valeur via le Serial:
    - `I:{valeur}` pour la valeur du courant en mA
    - `V:{valeur}` pour la valeur de la tension en Volt

On peut contrôler l'état de l'Arduino en envoyant les commandes suivantes:
- `S`: passage à l'état -1 (arrêt du moteur ou de la mesure)
- `M` : passage à l'état 0 (montée de la plateforme)
- `D` : passage à l'état 1 (descente de la plateforme)
- `C` : (comme *calcul*) passage à l'état 2 (mesure de l'intensité et la tension)

## Programme Arduino
Le programme Arduino est dans le dossier `Arduino/resistivite_nouveau`.
Il possède des fonctions qui ne sont pas utilisées dans le `main()` et qui peuvent être utilisées si un utilisateur souhaite faire quelques tests, ou bien réécrire le fonctionnement du programme pour ne pas utiliser le programme python.

## Programme python

### Bibliothèques utilisées
Utilise la bibliothèque `Tkinter` pour l'interface graphique, `uncertainties` pour le calcul de l'incertitude.
L'utilisation de la bibliothèque Tkinter rend le programme bien plus illisible.

Les bibliothèques nécessaires peuvent être installer via la commande suivante:
```
source python/setup.sh
```
(ou bien en écrivant les commandes contenues dans ce fichier). Cela permet d'installer les bibliothèques suivantes:
- `uncertainties` (pour la propagation des incertitudes)
- `ttkthemes` (pour avoir une interface plus jolie)
- `pyserial` (pour la communication Série avec Arduino)

S'il y a un problème avec la librairie `serial`, essayez de la désinstaller 
(`pip uninstall serial`) et d'installer `pyserial` (`pip install pyserial`)

### Compatibilité avec Windows ou Linux
Le suffixe du nom du port doit être changé dans le cas d'une utilisation sous linux.
Sur windows, la variable `suffix_port` (définie au début du programme python) est `'COM'`. Pour linux, je ne connais pas le nom des ports.

Par contre, je crois qu'il y a possibilité de trouver les ports actifs en utilisant la commande suivante:
```
import serial.tools.list_ports
ports = serial.tools.list_ports.comports(include_links=False)
print(ports)
```
(on pourrait peut-être utiliser cette fonctionnalitée pour avoir une liste déroutante de ports!)


## Branchement

### Alimentation

Lors du branchement du résistivomètre, il y a 4 câbles d'alimentation à brancher, et 4 fils à relier aux pointes conductrices.
- 2 câbles, l'un avec un connecteur rouge (pour le pôle positif), et l'autre avec un connecteur noir (pour le pôle négatif) permettent de fournir l'énergie au moteur. Ils sont donc reliés à une alimentation 12V. Les 2 fils reliés à leurs connecteurs sont bleus (on peut donc les reconnaître des 2 autres câbles).
- 2 autres câbles avec un connecteur rouge (pour le +) et noir (pour le -) permet d'imposer un courant entre 2 pointes conductrices. Ils sont donc connectés à un générateur de courant.

Voici un résumé des connecteurs à relier:

<img src="https://gitlab-student.centralesupelec.fr/2018correiaa/resistivometre_v1/-/raw/master/images/alimentation_branchement.png" width="400">



### Pointes conductrices

4 autres fils fins avec connecteurs femelles sont destinés à être reliés aux pointes conductrices
- Les fils rouge et noir correspondent aux pôles + et - (respectivement), par lesquellent un courant est imposé par le générateur de courant.
- les fils orange et jaune correspondent aux pôles + et - (respectivement) permettant la mesure de la tension.

Ainsi, voici les brancements à effectuer pour relier l'Arduino aux pointes (la couleur de la pointe sur ce graphique indique avec quel fil elle doit être reliée):

<img src="https://gitlab-student.centralesupelec.fr/2018correiaa/resistivometre_v1/-/raw/master/images/pointes_branchement.png" width="400">

