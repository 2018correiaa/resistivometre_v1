"""
Programme python avec interface graphique pour la mesure de la résistivité.
Communique avec l'Arduino via la voie Serial.

Fonctionne comme une machine d'état caractérisée par la variable `etat`:

* -1 : STOP
* 0 : MONTEE DU PLATEAU
* 1 : DESCENTE DU PLATEAU
* 2 : MESURE DE RESISTIVITE

Envoie le caractère suivant:

* 'S' pour STOP
* 'M' pour MONTEE
* 'D' pour DESCENTE
* 'C' pour MESURE
"""

suffix_port = "COM"

import time
import tkinter
import tkinter.ttk 
import serial
import math as m
from uncertainties import ufloat, ufloat_fromstr
from uncertainties import core
from ttkthemes import ThemedTk



## GLOBAL VARIABLES =====================================

temps_rafraichissement = 500 # ms, de la fenêtre
erreur_tension_V = 0.004 # 1 mV
erreur_courant_mA = 1 # 1 mA

quantities = ["courant", "tension", "résistivité"]

# Unités des `quantity`
unite_dict = {
    "courant": "mA",
    "tension": "V",
    "résistivité": "Ω.m",
    "épaisseur": "mm"
}

# Petit nom des `quantities` pour la communication avec l'Arduino
short_dict = {
    "I": "courant",
    "V": "tension",
    "rho": "résistivité",
    "e": "epaisseur"
}

erreur_dict = {
    "tension": 0.004,
    "courant": 1.,
}
# NB: erreur sur l'épaisseur récupérée via la zone de saisie,
# erreur sur la résistivité calculée par progagation.


# Associe un flottant avec incertitude (`ufloat`) à une `quantity`
valeur_dict = {
    "courant": ufloat(0., erreur_dict["courant"]),
    "tension": ufloat(0., erreur_dict["tension"]),
    "résistivité": ufloat(0., 0.),
    "épaisseur": ufloat(0.1, 0.),
}

## VARIABLES D'ETATS, initial ===========================
etat = -1



## GLOBAL FUNCTIONS =====================================

def is_ufloat(variable):
    """ Teste si une variable est un `ufloat`,
    c'est à dire un objet de la bibliothèque `uncertainties`,
    dont on peut accéder à

    * la valeur nominale via: variable.nominal_value
    * l'erreur via: variable.std_dev

    Parameters
    ----------
    variable:  variable
        variable

    Returns
    -------
    is_ufloat: bool
        `True` si `variable` est un `ufloat`, `False` sinon.
    """
    return isinstance(variable, core.Variable) or isinstance(variable, core.AffineScalarFunc)
    

def mise_en_forme_texte(valeur):
    """
    Remplace +/- par ± et limite
    l'affichage de `valeur` à 2 chiffres
    significatifs.
    Cette fonction est utilisée pour l'affichage
    de valeurs dans les zones de textes
    (e.g., intensité, résistivité, ...)

    Parameters
    ----------
    valeur: ufloat ou float
        `ufloat` est un flottant avec incertitude.

    Returns
    -------
    text: str
        Valeur en format `str`, avec 2 chiffres significatifs,
        et avec  `+/-` remplacé par `±`.
    """
    texte = f"{valeur:.2g}"
    texte = texte.replace("+/-", " ± ")
    return texte

def update_quantity(new_valeur, quantity):
    """ Met à jour la valeur de la quantité `quantity`
    ainsi que sa valeur affichée dans la zone
    de texte, dans les dictionnaires

    * `valeur_dict`: associe à une `quantity` son `ufloat`
    * `var_valeur_dict` : associe à une `quantity` un `str`
        qui indique la valeur à affichée dans la zone de texte.

    Parameters
    ----------
    new_valeur: float ou ufloat
        nouvelle valeur
    quantity: float
        quantité physique dont il faut modifier
        la valeur
    """
    global valeur_dict
    global  var_valeur_dict
    #print(quantity)
    #print(type(new_valeur))
    #print("new_valeur", new_valeur)
    if isinstance(new_valeur, str):
        valeur_dict[quantity] = ufloat_fromstr(new_valeur)
        if quantity in erreur_dict:
            # Si l'erreur est constante, et donc donnée par `erreur_dict`,
            # on ajoute l'erreur à la valeur.
            valeur_dict[quantity].std_dev = erreur_dict[quantity]
    elif is_ufloat(new_valeur):
        valeur_dict[quantity] = new_valeur
    else:
        erreur = erreur_dict.get(quantity, 0.)
        valeur_dict[quantity] = ufloat(float(new_valeur), std_dev=erreur)

    var_valeur_dict[quantity].set(mise_en_forme_texte(valeur_dict[quantity]))
    

def update_resistivite():
    """
    Met à jour la résistivité en fonction des valeurs
    de tensions et courants actuelles
    """
    global valeur_dict
    resistivite = calcul_resistivite(
        valeur_dict['tension'], valeur_dict['courant'], valeur_dict["épaisseur"]
    )
    update_quantity(resistivite, "résistivité")
    
    
## CALCUL RESISTIVITE ===================================
def calcul_resistivite(tension_V, courant_A, epaisseur_m):
    """ Calcule la résistivité

    Parameters
    ----------
    tension_V: float ou ufloat
        tension, en Volt
    courant_A: float ou ufloat
        courant, en Ampère
    epaisseur_m: float ou ufloat
        épaisseur, en mètre

    Returns
    -------
    resistivite: float ou ufloat
        Resistivité en Ω.m 
        
    Notes
    -----
    on peut aussi utiliser courant en mA et épaisseur en mm
    (cela donne le même résultat).
    Par ailleurs, si les valeurs sont des `ufloat`, le résultat
    sera un `ufloat` et on obtient la résistivité avec incertitude.
    """

    C = m.pi / m.log(2)
    if courant_A.nominal_value==0.:
        return 0.
    else:
        return C * epaisseur_m * (tension_V / courant_A)
    
## SERIAL =============================================

ser = serial.Serial(baudrate=115200)

# Fenêtre tkinter =====================================


root = ThemedTk(theme="breeze")

root.resizable(width=False, height=False)
root.title("Résistivitomètre")


"""
Template:
(0)  COM               | ZONE DE TEXTE  | CONNEXION
(2)  ---------------------------------------------------
(3)  ---------------------------------------------------
(4)  MONTEE            |                | DESCENTE
(5)  ---------------------------------------------------
(6)  ---------------------------------------------------
(7)  Epaisseur (mm)    | ZONE DE TEXTE
(8)  BOUTON CALCUL     | BOUTON STOP    | BOUTON RESET
(9)  COURANT (mA)      | ZONE DE TEXTE
(10) Tension (V)       | ZONE DE TEXTE
(11) Résistivité (Ω.m) | ZONE DE TEXTE
"""

## (0) Connexion -----------------------------------------------------------------
row = 0

def command_connect_button():
    """ Effet de l'appuie sur le boutant connection
    """
    global ser

    if connect_button["text"] == "disconnect":
        # Si on est déjà connecté, on se déconnecte
        
        ser.close()
        ser.port = None
        connect_button["text"] = "connect"

        toggle_state_montee_descente(False)
        var_com_value["state"] = "normal"
        
    else:
        # Si on est pas connecté, on essaye de se connecter
        try:
            # Récupére la valeur du port dans la zone de texte
            port = int(var_com_value.get())
            
            ser.port = f'{suffix_port}{port}'
            ser.open()
            
            time.sleep(0.1)
            ser.write(bytes(f"S", 'UTF-8'))
            toggle_state_montee_descente(True)
            
            connect_button["text"] = "disconnect"
            var_com_value["state"] = "disabled"

        except:
            ser.port = None

# Label "COM"        
com_label = tkinter.ttk.Label(text=f"COM", anchor="w", width=15)
com_label.grid(row = 0);

# Zone de texte
var_com_value = tkinter.ttk.Entry(
    root, text="8",
)
var_com_value.grid(row=row, column=1)

# Bouton connexion
connect_button = tkinter.ttk.Button(text ='Connexion', command=command_connect_button, width=12)  
connect_button.grid(row=row, column=2)

# Séparateur (1 et 2)
row += 1
separator = tkinter.ttk.Separator(root, orient='horizontal')
separator.grid(row=row, column=0, columnspan=3, sticky='ew')
row += 1
separator = tkinter.ttk.Separator(root, orient='horizontal')
separator.grid(row=row, column=0, columnspan=3, sticky='ew')



## (3) Commande moteur -------------------------------------------------------

def toggle_state_montee_descente(enable=True, sauf_montee=False, sauf_descente=False):
    """ Désactive (si `enable=False`) ou active (sinon) les boutons
    descente, montee, calcul et reset.
    Les autres montées sont soient déjà désactivées, soit n'ont pas besoin d'être
    désactivées.

    Parameters
    ----------
    enable: bool
        si `True`, active les boutons, sinon les désactivent
    sauf_montee: bool
        Si `True`, ne touche pas au bouton montée
    sauf_descente: bool
        Si `True`, ne touche pas au bouton descente
    """
    state = "normal" if enable else "disable"
    if not sauf_descente:
        descente_button["state"] = state
    if not  sauf_montee:
        montee_button["state"] = state
    calcul_button["state"] = state
    reset_button["state"] = state

def command_montee_bouton():
    """
    Effet de l'appuie sur le bouton "Montée"

    * Si on est en mode STOP: passage de l'état 0 -> montée du moteur
    * Si on est en mode MONTEE: arrêt du moteur
    """
    if montee_button["text"]=="Stop": # mode MONTEE

        # arrêt du moteur
        command_stop_moteur_bouton()
        
    else: # mode STOP
        # On demande la montée du moteur
        global etat
        etat = 0
        ser.write(bytes("M", 'UTF-8'))
        #montee_button.config(relief=tkinter.SUNKEN)
        montee_button.state(['pressed'])
        toggle_state_montee_descente(enable=False, sauf_montee=True)
        connect_button["state"] = "disabled"
        montee_button["text"] = "Stop"
        


def command_descente_bouton():
    """
    Effet de l'appuie sur le bouton "Descente"

    * Si on est en mode STOP: passage de l'état 0 -> descente du moteur
    * Si on est en mode DESCENTE: arrêt du moteur
    """
    if descente_button["text"]=="Stop": # mode DESCENTE
        # arrêt du moteur
        command_stop_moteur_bouton()
        
    else: # mode STOP
        # On demande la descente du moteur
        global etat
        etat = 1
        ser.write(bytes("D", 'UTF-8'))
        #descente_button.config(relief=tkinter.SUNKEN)
        descente_button.state(['pressed'])
        toggle_state_montee_descente(False, sauf_descente=True)
        connect_button["state"] = "disabled"
        descente_button["text"] = "Stop"


def command_stop_moteur_bouton():
    """
    Effet de l'appuie sur le bouton "stop".

    On envoie `S` à l'Arduino.
    Il nous renverra S pour confirmer l'arrêt
    du moteur.
    """
    ser.write(bytes("S", 'UTF-8'))
    
def effet_stop():
    """ Action à effectuer si l'Arduino
    envoie `'S'`, ce qui signifie que le moteur a été arrêté.

    On réactive quelques boutons.
    """
    global etat
    etat = -1
    montee_button.state(['!pressed'])
    montee_button.state(['!pressed'])
    montee_button["text"] = "Montée"
    descente_button["text"] = "Descente"
    
    toggle_state_montee_descente(True)
    connect_button["state"] = "normal"
    

# Bouton montée
row += 1
montee_button = tkinter.ttk.Button(text ='Montée', command=command_montee_bouton, width=12)  
montee_button.grid(row=row)
montee_button["state"] = "disabled"

# Bouton descente
descente_button = tkinter.ttk.Button(text ='Descente', command=command_descente_bouton, width=12)  
descente_button.grid(row=row, column=2)
descente_button["state"] = "disabled"

# Séparateur (5 et 6)
row += 1
separator = tkinter.ttk.Separator(root, orient='horizontal')
separator.grid(row=row, column=0, columnspan=3, sticky='ew')
row += 1
separator = tkinter.ttk.Separator(root, orient='horizontal')
separator.grid(row=row, column=0, columnspan=3, sticky='ew')

## (7-11) Calcul ----------------------------------------------------------------

def command_calcul_bouton():
    """
    Effet de l'appuie sur le bouton "calcul".
    Passage à l'état 2 à Calcul de la résistivité
    """
    entry = var_valeur_dict["épaisseur"].get()
    update_quantity(entry, "épaisseur")
    epaisseur_nominal = valeur_dict["épaisseur"].nominal_value
    #print(f"C:{entry}")
    ser.write(bytes(f"C:{epaisseur_nominal}", 'UTF-8'))
    global etat
    etat = 2
    #calcul_button.config(relief=tkinter.SUNKEN)
    calcul_button.state(['pressed'])
    
    stop_button["state"] = "normal"
    
def command_stop_bouton():
    """
    Effet de l'appuie sur le bouton STOP
    Passage à l'état -1 (on ne fait rien).
    """
    #print("S")
    ser.write(bytes("S", 'UTF-8'))
    global etat
    etat = -1
    #calcul_button.config(relief=tkinter.RAISED)
    calcul_button.state(['!pressed'])
    stop_button["state"] = "disable"

    

def command_reset_bouton():
    """
    Effet de l'appuie sur le bouton RESET
    Remise à 0 de la tension, du courant,
    et de la résistivité et mise en STOP.
    """

    command_stop_bouton()
    for quantity in ["tension", "courant", "résistivité"]:
        update_quantity(0.0, quantity)
    

# (8) bouton calcul et bouton stop .....................................................
row += 2
calcul_button = tkinter.ttk.Button(text ='Calcul', command=command_calcul_bouton, width=12)  
calcul_button.grid(row=row)

stop_button = tkinter.ttk.Button(text ='Stop', command=command_stop_bouton, width=12)  
stop_button.grid(row=row, column=1)
stop_button["state"] = "disable"
reset_button = tkinter.ttk.Button(text ='Reset', command=command_reset_bouton, width=12)  
reset_button.grid(row=row, column=2)

## (7, 9, 10, 11) Epaisseur, tension, courant ............................................


label_dict = {}
var_valeur_dict = {}
label_valeur_dict = {}

# Mise à part pour l'épaisseur, les autres zones de textes ne sont pas modifiables.
state_dict = {
    "épaisseur": tkinter.NORMAL
}

for quantity in ["courant", "tension", "résistivité"]:
    state_dict[quantity] = "readonly"

quantities = ["épaisseur", None, "courant", "tension", "résistivité"]

# Petite boucle qui permet de définir les labels "épaisseur", "courant", "tension", "résistivité"
# Et les zones de texte associées.
row-=1
for quantity in quantities:
    if quantity:
        # [1] texte
        unite = unite_dict[quantity]
        label_dict[quantity] = tkinter.ttk.Label(text=f"{quantity.capitalize()} ({unite})", anchor="e", width=15)
        label_dict[quantity].grid(row=row)

        # [2] zone de texte indiquant la valeur
        var_valeur_dict[quantity] = tkinter.StringVar()
        update_quantity(valeur_dict[quantity], quantity)
        
        label_valeur_dict[quantity] = tkinter.ttk.Entry(
            root, text=var_valeur_dict[quantity], state=state_dict[quantity]
        )
        label_valeur_dict[quantity].grid(row=row, column=1, columnspan=2)
        
    row += 1

toggle_state_montee_descente(False)


## BOUCLE ==================================================



def mettre_a_jour():
    """ Cette fonction est répétée toutes les 0.5 secs.
    Elle permet de lire les informations envoyée par l'Arduino,
    comme le résultat de la mesure en intensité et en courant.

    * Si l'état est -1, ne fait rien.
    * Si l'état est 0 ou 1, vérifie si `'S'` a été envoyé par l'Arduino,
        c'est-à-dire si le moteur a été arrêté
    * Si l'état est 2, récupère les nouvelles valeurs de tension
        et courant.
    """
    if ser.port: # Si on est connectée à l'Arduino
        
        if etat == -1: # Si état stop
            while(ser.in_waiting > 0):
                # on se moque de ce qu'envoie l'Arduino
                data = ser.readline()[:-2].decode()

        if etat==0 or etat==1: # descente ou montée du moteur
            # Si l'arduino renvoie 'S', cela signifie
            # que le moteur s'est arrêté.
            # comme cela a été demandé.
            while(ser.in_waiting > 0):
                data = ser.readline()[:-2].decode()
                #print(data)
                if data=="S":
                    effet_stop()
            
        
        if etat==2: # Mesure
            # On met à jour la mesure de la résistivite
            #print("Mise à jour de la mesure")
            while(ser.in_waiting > 0):
                # Lit la ligne
                data = ser.readline()[:-2].decode()
                #print(data)
                if len(data)!=0:
                    if data[0] in short_dict:
                        # Le premier caractère permet de savoir
                        # la quantité associée à la valeur qui est donnée
                        # La correspondance est faite via le dictionnaire
                        # `short_dict`
                        quantity = short_dict[data[0]]
                        # Le deuxième caractère est `:`
                        # et le reste des caractères est la nouvelle valeur(float)
                        update_quantity(data[2:], quantity)

            update_resistivite()
                    
        
    
    root.after(temps_rafraichissement, mettre_a_jour)



root.after(temps_rafraichissement, mettre_a_jour)
root.mainloop()
